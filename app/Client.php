<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Client extends Model
{
    protected $primaryKey = '_id';
    protected $collection = 'clients';
}
