<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Service\AccountManager;
use Illuminate\Support\Facades\DB;



class TenantServiceProvider extends ServiceProvider
{
     /**
     * creating the main log folder once installing the package.
     *
     */    

    public function boot()
    {
        $accountManager = new AccountManager;
        $accountManager->initAccount();
        dd(DB::connection()->getConfig());
    }

    
}