<?php

namespace App\Http\Controllers;

use App\UserDatabase;
use App\Http\Service\AccountManager;


class DatabasePointer extends Controller
{
    protected $accountManager;

    public function __construct(
        AccountManager $accountManager
    ){
        $this->accountManager = $accountManager;
    }

    public function create()
    {
        $accountManager = $this->accountManager->createNewAccount();

        $dataBaseConfig = UserDatabase::first();    
        dd($dataBaseConfig);
        dd(DB::connection()->getConfig());
    }
    
}