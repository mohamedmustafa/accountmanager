<?php

namespace App\Http\Service;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Client;
use App\UserDatabase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;


class AccountManager 
{   
    protected $config = [];

    public function createNewAccount()
    {
        $this->setDefaultConnection();
        $this->sendNewSubInfo();
        $this->getCurrentSubDatabaseAndSetConfig();
        $this->createUserCollectionForNewDatabase();
    }

    public function initAccount()
    {
        $this->setDefaultConnection();
        $this->getCurrentSubDatabaseAndSetConfig();
    }


    public function setCurrentDatabaseConfig($config)
    {
        $this->config = $config;
    }

    public function setCurrentConfigDatabase()
    {
        
        $dataBaseConfig = Client::where('name', $this->getSubdomain())->first(); 
        
        
        if(!empty($dataBaseConfig->name)){   
            $defaultDatabaseConfig=[
                'database.connections.tenant.username' => ( !empty($dataBaseConfig->mongo_user) ? $dataBaseConfig->mongo_user : ''),
                'database.connections.tenant.password' => ( !empty($dataBaseConfig->mongo_password) ? $dataBaseConfig->mongo_password : ''),
                'database.connections.tenant.database' => ( !empty($dataBaseConfig->database_name) ? $dataBaseConfig->database_name : ''),
                'database.connections.tenant.options.db' => '127.0.0.1',
                'database.connections.tenant.options.options.database' => '',
                'jwt.secret' => 'http://wildcardrouter.com',
                'database.default' => 'tenant'
            ];
        }
        
        foreach($defaultDatabaseConfig as $keyDefaultConfig => $valueDefaultConfig){
            \Config::set($keyDefaultConfig, $valueDefaultConfig);
        }
        
        DB::purge('tenant');
        DB::reconnect('tenant');
 
    }

    
    public function setDefaultConnection()
    {
        $config['database'] = env('DB_DATABASE');
        $config['username'] = env('DB_USERNAME');
        $config['password'] = env('DB_PASSWORD');
        $config['port'] = env('DB_PORT');

        $this->setCurrentDatabaseConfig($config);

        $this->excuteDatabase();
    }

    public function excuteDatabase(){
        $defaultDatabaseConfig=[
            'database.connections.tenant.username' => $this->config['username'],
            'database.connections.tenant.password' => $this->config['password'],
            'database.connections.tenant.database' => $this->config['database'],
            'database.connections.tenant.options.db' => '127.0.0.1',
            'database.connections.tenant.options.options.database' => '',
            'jwt.secret' => 'http://wildcardrouter.com',
            'database.default' => 'tenant'
        ];
        
        foreach($defaultDatabaseConfig as $keyDefaultConfig => $valueDefaultConfig){
            \Config::set($keyDefaultConfig, $valueDefaultConfig);
        }
        
        DB::purge('tenant');
        DB::reconnect('tenant');
    }

    public function sendNewSubInfo(){
        $user = [
            'name' => $this->getSubdomain(),
            'database_name' => $this->getSubdomain(),
            'mongo_password' => $this->config['password'],
            'mongo_user' => $this->config['username'],
            'mongo_db' => $this->getSubdomain(),
            'mongo_port' => '27017',
            'jwt_secret' => '',
            'email' => '',
            'status' => (int) 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'expiry_date_trail' => (int)Carbon::now()->addDays(env('trail_days', 15))->getTimestamp()
        ];
        
        (new Client())->insert($user);
    }

    public function getCurrentSubDatabaseAndSetConfig()
    {
        $account = $this->getSubdomain();
        $dataBaseConfig = Client::where('name', $account)->first();

        $config['database'] = (!empty($dataBaseConfig->database_name) ? $dataBaseConfig->database_name: '');
        $config['username'] = (!empty($dataBaseConfig->mongo_user) ? $dataBaseConfig->mongo_user: '');
        $config['password'] = (!empty($dataBaseConfig->mongo_password) ? $dataBaseConfig->mongo_password: '');
        $config['port'] = env('DB_PORT');

        $this->setCurrentDatabaseConfig($config);
        $this->excuteDatabase();
    }

    public function getSubdomain()
    {
        $currentUrl = URL::current();

        $account = explode('//', explode('.',  $currentUrl)[0])[0];
        if(isset($account[1])) {
            $account = explode('//', explode('.',  $currentUrl)[0])[1];
        }

        return $account;
    }

    public function createUserCollectionForNewDatabase()
    {
        if(!Schema::hasTable('users5')){
            Schema::create('users5', function ($collection) {
                $collection->index(
                    'username',
                    null,
                    null,
                    [
                        'sparse' => true,
                        'unique' => false,
                        'background' => true,
                    ],
                );
                $collection->index(
                    'password',
                    null,
                    null,
                    [
                        'sparse' => true,
                        'unique' => false,
                        'background' => true,
                    ],
                );
            });

        }

        $superAdmin = [
            'username' => 'mohamedmostafa',
            'password' => '123123'
        ];

        (new UserDatabase())->insert($superAdmin);
    }
    
}