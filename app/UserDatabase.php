<?php
namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class UserDatabase extends Model
{
    protected $primaryKey = '_id';
    protected $collection = 'users5';
}